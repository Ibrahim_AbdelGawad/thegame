package com.AppRocks.jackpot.services;



import javax.crypto.spec.OAEPParameterSpec;

import android.annotation.SuppressLint;
import android.app.Service;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.os.IBinder;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.AppRocks.jackpot.R;
import com.AppRocks.jackpot.activities.Main;
import com.nineoldandroids.animation.Animator;
import com.nineoldandroids.animation.ObjectAnimator;
import com.nineoldandroids.animation.ValueAnimator;
import com.nineoldandroids.animation.Animator.AnimatorListener;



public class ServiceTutorials extends Service implements OnClickListener 
{
	private String TAG = "ServiceTutorials";
	
	
	Animation animEnterHand;
	Animation animEnterCircle;
	Animation animMoveHand;
	Animation animScaleLine;	
	Animation animDisappear;

	
	ImageView imglblazkarType;
	ImageView imglblazkarIcon;
	
	
	View rowView;
	WindowManager localWindowManager;
	WindowManager.LayoutParams ll_lp;
	
	LayoutInflater inflater ;




	private Button btnClickMe;


	private int count;



	
	

	
	@Override
	public void onCreate() 
	{		
		super.onCreate();

		
		inflater = (LayoutInflater)getSystemService(LAYOUT_INFLATER_SERVICE);
		localWindowManager = (WindowManager)getSystemService(WINDOW_SERVICE);
				
		Log.d(TAG , "service created");
	}
	
		
	@SuppressLint("NewApi")
	@Override
	public void onStart(Intent intent, int startId) 
	{
		Log.d(TAG , "service Started");				
		super.onStart(intent, startId);
		
//		s = new SideBarParameters(this);
		// if service is restarted
		try 
		{
			localWindowManager.removeView(rowView);
		} 
		catch (Exception e) 
		{
			Log.d("Main", "ERROR :"+e.toString());			
		}
		
		rowView = inflater.inflate(R.layout.tutrials_left, null, false);
		
		                                       
		//Just a sample layout parameters.
		ll_lp = new WindowManager.LayoutParams();
		ll_lp.format = PixelFormat.TRANSLUCENT;
		ll_lp.height = WindowManager.LayoutParams.MATCH_PARENT;
		ll_lp.width  = WindowManager.LayoutParams.MATCH_PARENT;
		ll_lp.gravity = true ? (Gravity.RIGHT | Gravity.CENTER_VERTICAL):(Gravity.LEFT | Gravity.CENTER_VERTICAL);

		//This one is necessary.
		ll_lp.type = WindowManager.LayoutParams.TYPE_PHONE;

		// Play around with these two.
		ll_lp.flags = WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL;
		ll_lp.flags = ll_lp.flags | WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE;
		ll_lp.flags = ll_lp.flags | WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH;

		// Play around with these two.
//		ll_lp.flags = WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL;
//		ll_lp.flags = ll_lp.flags | WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE;		
//		ll_lp.flags = ll_lp.flags | WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM;
//		ll_lp.flags = ll_lp.flags | WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH;
//		ll_lp.flags = ll_lp.flags | WindowManager.LayoutParams.FLAG_IGNORE_CHEEK_PRESSES;
		
//		if(s.getPostionUCB()=='u')
//			ll_lp.gravity = ll_lp.gravity|Gravity.TOP;
//		else if(s.getPostionUCB()=='c')
			ll_lp.gravity = ll_lp.gravity|Gravity.CENTER_VERTICAL;					
//		else if(s.getPostionUCB()=='b')
//			ll_lp.gravity = ll_lp.gravity|Gravity.BOTTOM;		
		
		localWindowManager.addView(rowView, ll_lp);				
		
		
		
        findViews();
        findAnimation();        
        
	}

	

	private void findAnimation() 
	{
		ObjectAnimator o1 = ObjectAnimator.ofFloat(btnClickMe, "translationX", -200, 200, 0, 0).setDuration(10000);
	    ObjectAnimator o2 = ObjectAnimator.ofFloat(btnClickMe, "translationY", -150, 150, 0, 0).setDuration(5000);
	    ObjectAnimator o3 = ObjectAnimator.ofFloat(btnClickMe, "rotationX", 0,360).setDuration(3000);
	    ObjectAnimator o4 = ObjectAnimator.ofFloat(btnClickMe, "rotationY", 0,180).setDuration(3000);
	    ObjectAnimator o5 = ObjectAnimator.ofFloat(btnClickMe, "rotation", 0, -90).setDuration(3000);
	    final ObjectAnimator o6 = ObjectAnimator.ofFloat(btnClickMe,"alpha", 1, 0.25f, 0).setDuration(2000);
	    
	    o1.setRepeatCount(3);
	    o1.setRepeatMode(ValueAnimator.REVERSE);

	    o2.setRepeatCount(6);
	    o2.setRepeatMode(ValueAnimator.REVERSE);
	    
	    o3.setRepeatCount(12);
	    o3.setRepeatMode(ValueAnimator.REVERSE);
	    
	    o4.setRepeatCount(12);
	    o4.setRepeatMode(ValueAnimator.REVERSE);
	    
	    o5.setRepeatCount(12);
	    o5.setRepeatMode(ValueAnimator.REVERSE);
	    
	    
	    o1.addListener(new AnimatorListener() {
			
			@Override
			public void onAnimationStart(Animator animation) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onAnimationRepeat(Animator animation) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onAnimationEnd(Animator animation) {
				o6.start();
				
			}
			
			@Override
			public void onAnimationCancel(Animator animation) {
				// TODO Auto-generated method stub
				
			}
		});
	    
	    o6.addListener(new AnimatorListener() {
			
			@Override
			public void onAnimationStart(Animator animation) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onAnimationRepeat(Animator animation) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onAnimationEnd(Animator animation) {
				stopSelf();
				
			}
			
			@Override
			public void onAnimationCancel(Animator animation) {
				
				
			}
		});
	    
	    o1.start();
	    o2.start();
	    o3.start();
	    o4.start();
	    o5.start();
		
        				
	}


	private void findViews()
    {
		
		
		
		btnClickMe = (Button) rowView.findViewById(R.id.btnClickMe);
		
		
		
		btnClickMe.setOnClickListener(this);
		
		
    	
    }
	
	


	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public void onDestroy() 
	{		
		super.onDestroy();
		
		try 
		{
			localWindowManager.removeView(rowView);
		} 
		catch (Exception e) 
		{
			Log.d("ServiceTutorials", "ERROR :"+e.toString());			
		}
		
		
		Log.d(TAG, "ServiceTutorials destroyed");
	}
	
	
	
	
	
	



	@Override
	public void onClick(View arg0) {
		btnClickMe.setText(""+count);
		count++;
		
	}

}