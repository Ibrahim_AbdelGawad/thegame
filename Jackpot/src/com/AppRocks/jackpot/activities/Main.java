package com.AppRocks.jackpot.activities;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.AppRocks.jackpot.JackpotParameters;
import com.AppRocks.jackpot.R;
import com.facebook.Session;
import com.makeramen.RoundedImageView;

@EActivity(R.layout.main)
public class Main extends Activity {

	
	@ViewById
	RelativeLayout Rlcontainer;
	
	@ViewById
	TextView txtSignOut;

	JackpotParameters p;

	@ViewById
	RoundedImageView imgJAckBot;

	@ViewById
	ImageView img3StageProgress;

	@ViewById
	Button btnPlay;

	@Click
	void btnPlay() {
		startActivity(new Intent(Main.this, Jackpot_.class));
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		p = new JackpotParameters(this);

	}
	
	@Override
	protected void onResume() {
		
		
		super.onResume();
	}
	
	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		if(hasFocus)
		{
			scaleContents(Rlcontainer);
		}
		super.onWindowFocusChanged(hasFocus);
	}

	@Click
	void txtSignOut() {
		callFacebookLogout(this);
		p.setBoolean(false, "login");
		p.setString("", "email");
		p.setString("", "fname");
		p.setString("", "lname");
		startActivity(new Intent(this, Login_.class));
		finish();
	}

	@Click
	void imgJAckBot() {
		// startActivity(new Intent(this, FullscreenDemoActivity.class));
	}

	public static void callFacebookLogout(Context context) {
		Session session = Session.getActiveSession();
		if (session != null) {

			if (!session.isClosed()) {
				session.closeAndClearTokenInformation();
				// clear your preferences if saved
			}
		} else {

			session = new Session(context);
			Session.setActiveSession(session);

			session.closeAndClearTokenInformation();
			// clear your preferences if saved

		}

	}

	private void scaleContents(View rootView) {
		// Compute the scaling ratio
		
//		float xScale = (float) container.getWidth() / rootView.getWidth();
//		float yScale = (float) container.getHeight() / rootView.getHeight();
		
		float xScale = (float) getResources().getDisplayMetrics().widthPixels / rootView.getWidth();
		float yScale = (float) getResources().getDisplayMetrics().heightPixels / rootView.getHeight();
		
		
		float scale = Math.min(xScale, yScale);
		// Scale our contents
		scaleViewAndChildren(rootView, scale);
	}

	// Scale the given view, its contents, and all of its children by the given
	// factor.
	public static void scaleViewAndChildren(View root, float scale) {
		// Retrieve the view's layout information
		ViewGroup.LayoutParams layoutParams = root.getLayoutParams();
		// Scale the view itself
		if (layoutParams.width != ViewGroup.LayoutParams.FILL_PARENT
				&& layoutParams.width != ViewGroup.LayoutParams.WRAP_CONTENT) {
			layoutParams.width *= scale;
		}
		if (layoutParams.height != ViewGroup.LayoutParams.FILL_PARENT
				&& layoutParams.height != ViewGroup.LayoutParams.WRAP_CONTENT) {
			layoutParams.height *= scale;
		}
		// If this view has margins, scale those too
		if (layoutParams instanceof ViewGroup.MarginLayoutParams) {
			ViewGroup.MarginLayoutParams marginParams = (ViewGroup.MarginLayoutParams) layoutParams;
			marginParams.leftMargin *= scale;
			marginParams.rightMargin *= scale;
			marginParams.topMargin *= scale;
			marginParams.bottomMargin *= scale;
		}
		// Set the layout information back into the view
		root.setLayoutParams(layoutParams);
		// Scale the view's padding
		root.setPadding((int) (root.getPaddingLeft() * scale),
				(int) (root.getPaddingTop() * scale),
				(int) (root.getPaddingRight() * scale),
				(int) (root.getPaddingBottom() * scale));
		// If the root view is a TextView, scale the size of its text
		if (root instanceof TextView) {
			TextView textView = (TextView) root;
			textView.setTextSize(textView.getTextSize() * scale);
		}
		// If the root view is a ViewGroup, scale all of its children
		// recursively
		if (root instanceof ViewGroup) {
			ViewGroup groupView = (ViewGroup) root;
			for (int cnt = 0; cnt < groupView.getChildCount(); ++cnt)
				scaleViewAndChildren(groupView.getChildAt(cnt), scale);
		}
	}

}
