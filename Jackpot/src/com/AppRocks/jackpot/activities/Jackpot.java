package com.AppRocks.jackpot.activities;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.AppRocks.jackpot.JackpotParameters;
import com.AppRocks.jackpot.R;
import com.AppRocks.jackpot.activities.youtube.FullscreenDemoActivity;
import com.AppRocks.jackpot.activities.youtube.FullscreenDemoActivity2;
import com.facebook.Session;
import com.makeramen.RoundedImageView;

@EActivity(R.layout.jackpot)
public class Jackpot extends Activity {

	@ViewById
	TextView txtSignOut;

	JackpotParameters p;

	@ViewById
	RoundedImageView imgJAckBot;

	@ViewById
	Button btnSeeTheVideo;
	@ViewById
	Button btnYouCanPlay;

	@Click
	void btnSeeTheVideo() {
		startActivity(new Intent(this, FullscreenDemoActivity2.class));
	}

	@Click
	void btnYouCanPlay() {
		startActivity(new Intent(this, GoodLuck.class));
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		p = new JackpotParameters(this);

	}

	@Click
	void txtSignOut() {
		callFacebookLogout(this);
		p.setBoolean(false, "login");
		p.setString("", "email");
		p.setString("", "fname");
		p.setString("", "lname");
		startActivity(new Intent(this, Login_.class));
		finish();
	}

	@Click
	void imgJAckBot() {
		// startActivity(new Intent(this, FullscreenDemoActivity.class));
	}

	public static void callFacebookLogout(Context context) {
		Session session = Session.getActiveSession();
		if (session != null) {

			if (!session.isClosed()) {
				session.closeAndClearTokenInformation();
				// clear your preferences if saved
			}
		} else {

			session = new Session(context);
			Session.setActiveSession(session);

			session.closeAndClearTokenInformation();
			// clear your preferences if saved

		}

	}

}
